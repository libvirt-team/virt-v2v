Source: virt-v2v
Section: admin
Priority: optional
Maintainer: Hilko Bengen <bengen@debian.org>
Build-Depends: debhelper-compat (= 13),
 pkgconf,
 ocaml-nox, ocaml-findlib, libguestfs-ocaml-dev, libgettext-ocaml-dev, libounit-ocaml-dev,
 libguestfs-perl,
 libguestfs-dev,
 libjansson-dev,
 libnbd-dev, libnbd-ocaml-dev,
 libosinfo-1.0-dev,
 libpcre2-dev,
 libvirt-ocaml-dev,
 libxml2-dev,
 libnbd-bin,
 xorriso,
 sqlite3,
 zip, unzip,
 bash-completion,
 po4a,
# libguestfs-tools <!nocheck>,
# linux-image-amd64 [amd64] <!nocheck>,
# jq <!nocheck>,
# libvirt-clients <!nocheck>,
Standards-Version: 4.5.1
Homepage: https://libguestfs.org/
Vcs-Browser: https://salsa.debian.org/libvirt-team/virt-v2v
Vcs-Git: https://salsa.debian.org/libvirt-team/virt-v2v.git
Rules-Requires-Root: no

Package: virt-v2v
Architecture: any
Depends: ${shlibs:Depends}, ${misc:Depends},
 libnbd-bin,
 nbdkit,
 nbdkit-plugin-python,
 qemu-utils,
Suggests: rhsrvany, nbdkit-vddk-plugin
Description: virtual-to-virtual machine converter
 Virt-v2v is a program that converts a single guest from a foreign
 hypervisor to run on KVM. It can read Linux and Windows guests
 running on VMware, Xen, Hyper-V and some other hypervisors, and
 convert them to KVM managed by libvirt, OpenStack, oVirt, Red Hat
 Virtualisation (RHV) or several other targets. It can modify the
 guest to make it bootable on KVM and install virtio drivers so it
 will run quickly.
